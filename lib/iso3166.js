const countriesRaw = {
  UA: ['Ukraine'],
  US: ['USA', 'Maryland', 'Uninted States', 'United States', 'U.S.A.', 'US'],
  NL: ['The Netherlands', 'Netherlands', 'Rotterdam'],
  PT: ['Portugal'],
  PL: ['Poland'],
  GB: ['United Kingdom', 'UK', 'England'],
  PE: ['Peru'],
  BR: ['Brazil'],
  FR: ['France'],
  ES: ['Spain'],
  DE: ['Germany'],
  CA: ['Canada'],
  SE: ['Sweden'],
  IL: ['Israel'],
  TW: ['Taiwan'],
  CL: ['Chile'],
  AU: ['Australia'],
  NG: ['Nigeria'],
  IN: ['India'],
  MY: ['Malaysia'],
  MX: ['Mexico'],
  RS: ['Serbia'],
  ZA: ['South Africa'],
  BE: ['Belgium'],
  CZ: ['Czech Republic'],
  CN: ['China'],
  IE: ['Ireland'],
  AT: ['Austria'],
  IT: ['Italy'],
  ZW: ['Zimbabwe'],
  JP: ['Japan'],
  SI: ['Slovenia'],
  EG: ['Egypt'],
  NI: ['Nicaragua'],
  RU: ['Russia', 'Russian Federation'],
  SK: ['Slovakia'],
  GR: ['Greece'],
  HU: ['Hungary'],
  LT: ['Lithuania'],
  MT: ['Malta'],
  NZ: ['New Zealand'],
  PH: ['Philippines'],
  BA: ['Bosnia and Herzegovina'],
  KE: ['Kenya'],
  PK: ['Pakistan'],
  LU: ['Luxembourg'],
  MN: ['Mongolia'],
  DK: ['Denmark'],
  BY: ['Belarus'],
  NO: ['Norway'],
  AR: ['Argentina'],
  MD: ['Moldova'],
  DO: ['Dominican Republic'],
  EC: ['Ecuador'],
  SG: ['Singapore'],
  RO: ['Romania'],
  AD: ['Andorra'],
  CH: ['Switzerland'],
  TR: ['Turkey'],
  CO: ['Colombia'],
  IS: ['Iceland'],
  KR: ['South Korea', 'Korea (South)'],
  CY: ['Cyprus'],
  VN: ['Vietnam'],
  MA: ['Morocco'],
  KH: ['Cambodia'],
  BG: ['Bulgaria'],
  LK: ['Sri Lanka'],
  PY: ['Paraguay'],
  CR: ['Costa Rica'],
  PA: ['Panama'],
  ID: ['Indonesia'],
  AO: ['Angola'],
  LV: ['Latvia']
};

const iso3166AlphaToCountryName = (code) => {
  if (!countriesRaw[code]) {
    throw new Error('Unknown country with code ' + code);
  }
  return countriesRaw[code][0];
};

const countryMap = Object.entries(countriesRaw).reduce((all, [key, entries]) => {
  return {
    ...all,
    [key.toLocaleLowerCase()]: key,
    ...Object.fromEntries(entries.map((x) => [x.toLocaleLowerCase(), key]))
  };
}, {});

const countryNameToIso3166Alpha = (country) => {
  const code = countryMap[country.toLocaleLowerCase()];

  if (!code) {
    throw new Error(`Unknown country: '${country}'`);
  }

  return code;
};

module.exports = {
  countryNameToIso3166Alpha,
  iso3166AlphaToCountryName
};
