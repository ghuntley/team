const fs = require('fs');
const path = require('path');

const _ = require('lodash');
const Promise = require('bluebird');

const axios = require('axios');
const querystring = require('qs');
const states = require('us-state-codes');
const yaml = require('yaml-js');

const HashIndex = require('./lib/hashIndex');
const { countryNameToIso3166Alpha, iso3166AlphaToCountryName } = require('./lib/iso3166');

const hashIndex = new HashIndex();

const TEAM_MAP_VERSION = 10;

if (!process.env.GEONAMES_USERNAME) {
  throw new Error('env variable GEONAMES_USERNAME must be set');
}

const geoNames =
  (endpoint) =>
  async (params = {}) => {
    const response = await axios({
      method: 'get',
      url: `https://secure.geonames.org/${endpoint}`,
      params: {
        type: 'json',
        username: process.env.GEONAMES_USERNAME,
        ...params
      },
      paramsSerializer: function (params) {
        return querystring.stringify(params, { arrayFormat: 'repeat' });
      }
    });

    const status = _.get(response, 'data.status', false);
    if (status) {
      throw new Error(
        `GeoNames raised error ${status.value}: ${status.message}. Search: ${response.request.path}`
      );
    }

    return _.get(response, 'data.geonames');
  };

const searchBlacklist = ['St. ', 'San '];

const startsWithBlackList = (name) => {
  if (!name) {
    return false;
  }

  if (searchBlacklist.some((b) => name.startsWith(b))) {
    return true;
  }

  return !/^[\S]+$/.test(name.substr(0, 3));
};

const searchGeoNames = geoNames('search');
const countryInfo = geoNames('countryInfo');

const STATE_CODE_REGEX = /,\s*([A-Z]+)\s*(,\s?USA?)?$/i;

function getLocality(locationName) {
  let search = (locationName && locationName.trim()) || '';

  search = search.slice(0, 1).toLocaleUpperCase() + search.slice(1);

  return search !== 'Anywhere' ? search : '';
}

function getCenterOfBoundingBox({ north, west, south, east }) {
  const [lat1, lng1, lat2, lng2] = [north, west, south, east].map(
    (f) => (parseFloat(f) * Math.PI) / 180
  );

  const x = Math.cos(lat1) * Math.cos(lng1) + Math.cos(lat2) * Math.cos(lng2);
  const y = Math.cos(lat1) * Math.sin(lng1) + Math.cos(lat2) * Math.sin(lng2);
  const z = Math.sin(lat1) + Math.sin(lat2);

  return [Math.atan2(z, Math.sqrt(x * x + y * y)), Math.atan2(y, x)].map(
    (f) => f * (180 / Math.PI)
  );
}

const getSearchQuery = async (locationName, country) => {
  const search = (locationName !== 'TBD' && locationName) || false;
  const countryCode = countryNameToIso3166Alpha(country);

  if (search) {
    let options = {
      maxRows: 1,
      inclBbox: true,
      featureClass: ['P', 'A'],
      country: countryCode,
      orderby: 'relevance',
      q: search,
      isNameRequired: true
    };

    if (countryCode === 'US' && STATE_CODE_REGEX.test(search)) {
      let state = search.match(STATE_CODE_REGEX)[1];
      options.q = search.replace(STATE_CODE_REGEX, '');

      if (state.length !== 2) {
        state = states.getStateCodeByStateName(states.sanitizeStateName(state));
      }

      if (state.length === 2) {
        options.adminCode1 = state.toUpperCase();
      }
    }

    //Ensure that abbreviations / spaces are not used for startsWith
    if (!startsWithBlackList(search)) {
      options.name_startsWith = search.substr(0, 3);
    }

    const result = _.get(await searchGeoNames(options), '[0]', {});

    return { ...result, countryCode };
  }

  const result = _.get(await countryInfo({ country: countryCode }), '[0]', {});

  const [lat, lng] = getCenterOfBoundingBox(result);

  return { ...result, lat, lng, countryCode };
};

const cachedLocations = {
  'Munich|Germany': {
    location: [48.135124, 11.581981],
    locality: 'Munich',
    country: 'Germany'
  },
  'Kiel|Germany': {
    location: [54.323292, 10.122765],
    locality: 'Kiel',
    country: 'Germany'
  },
  'Hamburg|Germany': {
    location: [53.551085, 9.993682],
    locality: 'Hamburg',
    country: 'Germany'
  },
  'Lippstadt|Germany': {
    location: [51.676380, 8.346650],
    locality: 'Lippstadt',
    country: 'Germany'
  },
  'Athens|Greece': {
    location: [37.983810, 23.727539],
    locality: 'Athens',
    country: 'Greece'
  },
  'Versailles|France': {
    location: [48.802200, 2.129680],
    locality: 'Versailles',
    country: 'France'
  },
  'Lahore|Pakistan': {
    location: [31.52037,74.358747],
    locality: 'Lahore',
    country: 'Pakistan'
  },
  'Dortmund|Germany': {
    location: [51.513587,7.465298],
    locality: 'Dortmund',
    country: 'Germany'
  },
  'London|UK': {
    location: [51.507218,-0.127586],
    locality: 'London',
    country: 'UK'
  },
  'Berlin|Germany': {
    location: [52.520007,13.404954],
    locality: 'Berlin',
    country: 'Germany'
  },
  'Vancouver|Canada': {
    location: [49.282729,-123.120737],
    locality: 'Vancouver',
    country: 'Canada'
  },
  'Sydney|Australia': {
    location: [-33.86882,151.209296],
    locality: 'Sydney',
    country: 'Australia'
  },
  'Santiago|Chile': {
    location: [-33.44889,-70.669265],
    locality: 'Santiago',
    country: 'Chile'
  },
  'Karlsruhe|Germany': {
    location: [49.00689,8.403653],
    locality: 'Karlsruhe',
    country: 'Germany'
  },
  'Copenhagen|Denmark': {
    location: [55.676097,12.568337],
    locality: 'Copenhagen',
    country: 'Denmark'
  },
  'Bengaluru|India': {
    location: [12.971599,77.594563],
    locality: 'Bengaluru',
    country: 'India'
  },
  'Quiaios|Portugal': {
    location: [40.225086,-8.847302],
    locality: 'Quiaios',
    country: 'Portugal'
  },
  'Leeds|UK': {
    location: [53.800755,-1.549077],
    locality: 'Leeds',
    country: 'UK'
  },
  'Telford|UK': {
    location: [52.677587,-2.467261],
    locality: 'Telford',
    country: 'UK'
  },
  'Prague|Czech Republic': {
    location: [50.075538,14.437801],
    locality: 'Prague',
    country: 'Czech Republic'
  },
  'Lima|Peru': {
    location: [-12.046373,-77.042754],
    locality: 'Lima',
    country: 'Peru'
  },
  'San Fransisco|USA': {
    location: [37.77493,-122.419415],
    locality: 'San Fransisco',
    country: 'USA'
  },
  'Coventry|USA': {
    location: [41.688598,-71.564208],
    locality: 'Coventry',
    country: 'USA'
  },
  'Guangzhou|China': {
    location: [23.12908,113.26436],
    locality: 'Guangzhou',
    country: 'China'
  },
  'Guangzhou|China': {
    location: [23.12908,113.26436],
    locality: 'Guangzhou',
    country: 'China'
  },
  'Pforzheim|Germany': {
    location: [48.892186,8.694629],
    locality: 'Pforzheim',
    country: 'Germany'
  },
  'Düsseldorf|Germany': {
    location: [51.227741,6.773456],
    locality: 'Düsseldorf',
    country: 'Germany'
  },
  'Dhaka|Bangladesh': {
    location: [23.810332,90.412518],
    locality: 'Dhaka',
    country: 'Bangladesh'
  },
  'Nuremberg|Germany': {
    location: [49.452102,11.076665],
    locality: 'Nuremberg',
    country: 'Germany'
  },
  'Kanagawa|Japan': {
    location: [35.491354,139.284143],
    locality: 'Kanagawa',
    country: 'Japan'
  },
  'Los Angeles|USA': {
    location: [34.052234,-118.243685],
    locality: 'Los Angeles',
    country: 'USA'
  },
  'Tallinn|Estonia': {
    location: [59.436961,24.753575],
    locality: 'Tallinn',
    country: 'Estonia'
  },
  'Aachen|Germany': {
    location: [50.775346,6.083887],
    locality: 'Aachen',
    country: 'Germany'
  },
  'Hyperabad|India': {
    location: [17.385044,78.486671],
    locality: 'Hyperabad',
    country: 'India'
  },
  'Seattle|USA': {
    location: [47.60621,-122.332071],
    locality: 'Seattle',
    country: 'USA'
  },
  'Cambridge|UK': {
    location: [52.195079,0.131273],
    locality: 'Cambridge',
    country: 'UK'
  },
  'Melbourne|Australia': {
    location: [-37.813628,144.963058],
    locality: 'Melbourne',
    country: 'Australia'
  },
  'Stralsund|Germany': {
    location: [54.309065,13.077035],
    locality: 'Stralsund',
    country: 'Germany'
  },
  'Milan|Italy': {
    location: [45.464204,9.189982],
    locality: 'Milan',
    country: 'Italy'
  },
};

const roundNumber = (number) => +parseFloat(number).toFixed(3);

const getLocation = async (locationName, country) => {
  const cacheKey = `${locationName}|${country}`;

  if (cachedLocations[cacheKey]) {
    console.log(`Using cached coordinates for ${locationName} in ${country}`);
    return cachedLocations[cacheKey];
  } else {
    console.log(`'${cacheKey}' not cached`);
  }

  const { lat, lng, countryCode, adminCode1, name } =
    (await getSearchQuery(locationName, country)) || {};

  if (!lng || !lat) {
    throw new Error(`Could not find ${locationName} in ${country}`);
  }

  cachedLocations[cacheKey] = {
    location: [roundNumber(lat), roundNumber(lng)],
    countryCode,
    adminCode1,
    locality: name,
    country: iso3166AlphaToCountryName(countryCode)
  };

  return cachedLocations[cacheKey];
};

let cached = [];

try {
  const previous = JSON.parse(fs.readFileSync('./team.json', 'utf8'));
  if (_.get(previous, 'version') === TEAM_MAP_VERSION) {
    cached = previous.team;
  }
} catch (e) {
  console.log('Could not load existing reverse coded team.json');
}

const memberPictureBaseURL = 'github.com/';

function getMemberPicture(picture) {
  if (picture.startsWith('https://')) {
    return picture;
  }

  const pictureURL = 'https://' + path.normalize(`${memberPictureBaseURL}${picture}`);

  if (pictureURL.includes(memberPictureBaseURL)) {
    return `${pictureURL.replace(/\.(png|jpe?g)$/gi, '')}.png`;
  }

  return pictureURL;
}

function compareMembers(a, b) {
  const keyLength = Math.min(a.key.length, b.key.length);

  return a.key.substr(0, keyLength) === b.key.substr(0, keyLength);
}

const mapMember = async (member) => {
  const hash = hashIndex.shorten(member.key);

  if (cached[hash]) {
    console.log(`Cached result for ${member.name}`);
    return cached[hash];
  }

  console.log(
    `Searching location for ${member.name}: ${member.locality || 'No locality'} in ${
      member.country
    }`
  );

  let result;
  try {
    result = await getLocation(member.locality, member.country);
  } catch (e) {
    throw new Error(`Could not load location for ${member.name}:\n\t${e}`);
  }

  const { location, countryCode, adminCode1, locality, country } = result;
  console.log(
    `${member.name}; ${member.locality || 'No locality'}, ${
      member.country
    }: ${location} — ${country}`
  );

  return {
    key: hash,
    name: member.name,
    location,
    countryCode,
    stateCode: countryCode === 'US' ? adminCode1 : null,
    locality,
    country,
    picture: member.picture
  };
};

function bail(e) {
  if (e && e.stack) {
    console.warn(e.stack);
  }
  console.warn(`Error creating the team page map:\n\t${e}`);
  process.exit(1);
}

// Get document, or throw exception on error
async function main() {
  const doc = yaml.load(fs.readFileSync('./team.yml', 'utf8'));

  const members = doc
    .filter(
      (member) =>
        member.type !== 'vacancy' &&
        member.country &&
        member.country !== 'Remote'
    )
    .flatMap((member) => {
      const locality = getLocality(member.locality);
      const picture = getMemberPicture(member.picture);
      const res = { ...member, locality, picture };
      const countryCode = countryNameToIso3166Alpha(member.country);
      if(['RU', 'BY', 'UA'].includes(countryCode)){
        return []
      }
      res.key = hashIndex.create(member);
      return res;
    })
    .sort(function (a, b) {
      if (a.start_date > b.start_date) {
        return 1;
      }
      if (a.start_date < b.start_date) {
        return -1;
      }
      return 0;
    });
  
  const tooMuch = _.differenceWith(cached, members, compareMembers);

  if (tooMuch.length > 0) {
    console.log(`Going to remove ${tooMuch.map((x) => x.name).join(', ')}`);
    cached = _.difference(cached, tooMuch);
  }

  cached = _.keyBy(cached, 'key');

  const team = await Promise.map(members, mapMember, { concurrency: 1 });

  console.log('\nFound a location for all team members');
  const result = { version: TEAM_MAP_VERSION, team };
  return fs.writeFileSync('./team.json', JSON.stringify(result));
}

main()
  .then(() => {
    console.log('Mapped all members on the team page and wrote results to team.json');
    process.exit(0);
  })
  .catch(bail);
