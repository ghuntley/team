# Gitpod Team Map

> Map of the Gitpod team all around the globe.

🗺️ [Open the map](https://gtsiolis.gitlab.io/team/)

## Background

This is a fork of the [`gitlab-com/teampage-map`](https://gitlab.com/gitlab-com/teampage-map) project.

## Credits

Thanks to [@leipert](https://gitlab.com/leipert) for creating and maintainng the original fork.

## License

MIT © 2018-present GitLab B.V.

[team.yml]: https://gitlab.com/gitlab-com/www-gitlab-com/blob/master/data/team.yml
[standard-readme]: https://github.com/RichardLitt/standard-readme
